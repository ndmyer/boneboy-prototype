﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    public Animator skelAnim;


    // Determines Which Animation Will Play
    int noOfClicks; 

    // Locks ability to click during animation event
    bool canClick; 

    // Length of Animations
    private float qaLength1 = .28f;
    private float qaLength2 = 0.5f;
    private float qaLength3 = 0.75f;

    // Time LMB has been held
    public float swordChargeTimer = 0f;

    void Start()
    {
        noOfClicks = 0;
        canClick = true;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        { // Increases swordChargeTimer based on time LMB is held
            swordChargeTimer += Time.deltaTime;
        }
        
        if ((Input.GetMouseButtonUp(0)) && (swordChargeTimer >= 1))
        { // When LMB is released if swordChargeeTimer is at or above 1 second 
          // Resets swordChareTimer to 0 and plays Charge Attack animation
            swordChargeTimer = 0;
            skelAnim.SetInteger("LightCombo", 4);
            Debug.Log("Charge Atk");
        }
        else if (Input.GetMouseButtonUp(0))
        { // When LMB is released and swordChargeTimer is less than 1
          // Resets swordChareTimer to 0 and initiates LightComboStarter function
            swordChargeTimer = 0;
            LightComboStarter();
        }

        if (Input.GetMouseButtonUp(1))
        { // Plays Heavy Trigger on RMB
            skelAnim.SetTrigger("HeavyAtk");
        }
    }

    void LightComboStarter()
    {
        if (canClick)
        {
            noOfClicks++;
        }

        if (noOfClicks == 1)
        {
            skelAnim.SetInteger("LightCombo", 1);
            //Debug.Log("QuickAttack1");
            StartCoroutine(QuickAttack1());
        }
    }

    IEnumerator QuickAttack1()
    {
        Debug.Log("Coroutine1");
        yield return new WaitForSeconds(qaLength1);
        Debug.Log(" LightComboCheck1");
        LightComboCheck();
    }

    IEnumerator QuickAttack2()
    {
        Debug.Log("Coroutine2");
        yield return new WaitForSeconds(qaLength2);
        Debug.Log(" LightComboCheck2");
        LightComboCheck();
    }

    IEnumerator QuickAttack3()
    {
        Debug.Log("Coroutine3");
        yield return new WaitForSeconds(qaLength3);
        Debug.Log(" LightComboCheck3");
        LightComboCheck();
    }


    public void LightComboCheck()
    {

        //canClick = false;
        Debug.Log("LightComboCheck");

        if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Player_QuickAttack1") && noOfClicks == 1)
        { // If the first animation is still playing and only 1 click has happened, return to idle
            skelAnim.SetInteger("LightCombo", 0);
            canClick = true;
            noOfClicks = 0;
            Debug.Log("Idle1");
        }
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Player_QuickAttack1") && noOfClicks >= 2)
        {// If the first animation is still playing and at least 2 clicks have happened, continue the combo          
            skelAnim.SetInteger("LightCombo", 2);
            canClick = true;
            Debug.Log("QuickAttack2");
            StartCoroutine(QuickAttack2());
        }
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Player_QuickAttack2") && noOfClicks == 2)
        { // If the second animation is still playing and only 2 clicks have happened, return to idle          
            skelAnim.SetInteger("LightCombo", 0);
            canClick = true;
            noOfClicks = 0;
            Debug.Log("Idle2");
        }
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Player_QuickAttack2") && noOfClicks >= 3)
        { // If the second animation is still playing and at least 3 clicks have happened, continue the combo          
            skelAnim.SetInteger("LightCombo", 3);
            canClick = true;
            Debug.Log("QuickAttack3");
            StartCoroutine(QuickAttack3());
        }
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Player_QuickAttack3"))
        { // Since this is the third and last animation, return to idle          
            skelAnim.SetInteger("LightCombo", 0);
            canClick = true;
            noOfClicks = 0;
            Debug.Log("Idle3");
        }
        else
        { // Catch all just in case
            skelAnim.SetInteger("LightCombo", 0);
            canClick = true;
            noOfClicks = 0;
            Debug.Log("Idle4");
        
        }
        /*
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Low Slash 33") && noOfClicks >= 3)
        {  //If the second animation is still playing and at least 3 clicks have happened, continue the combo         
            skelAnim.SetInteger("LightCombo", 6);
            canClick = true;
        }
        else if (skelAnim.GetCurrentAnimatorStateInfo(0).IsName("Spin Slash 6"))
        { //Since this is the third and last animation, return to idle          
            skelAnim.SetInteger("LightCombo", 4);
            canClick = true;
            noOfClicks = 0;
        }*/
    }
}


