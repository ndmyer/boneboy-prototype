﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    CharacterController characterController;

    public Camera playerCamera;

    public Animator skelAnim;

    [Range(1f, 10f)]
    public float speedMult = 1f;
    public float gravity = 20.0f;

    private float speed = 1.0f;

    GameObject targetObject;
    public GameObject targetIndicatorPrefab;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 axisVector = Vector3.zero;
    private Vector3 mousePos = Vector3.zero;

    Vector2 playerPosOnScreen;
    Vector2 cursorPosition;
    Vector2 offsetVector;

    Plane surfacePlane = new Plane();

    private void Awake()
    {
        // Instantiate aim target prefab
        if (targetIndicatorPrefab)
        {
            targetObject = Instantiate(targetIndicatorPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        }

        // Hide the cursor
        Cursor.visible = false;
    }

    void Start()
    {
        // Gets Character Controller Component 
        characterController = GetComponent<CharacterController>();

        skelAnim.SetFloat("WalkMult", speedMult);
        speed = speed * speedMult;
    }

    void Update()
    {
        // Checks if player is grounded
        if (characterController.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes
            float moveX = Input.GetAxis("Horizontal");
            float moveZ = Input.GetAxis("Vertical");

            axisVector = new Vector3(moveX, 0, moveZ);
            moveDirection = new Vector3(moveX, 0, moveZ).normalized;
            //skelAnim.SetFloat("WalkMult", speedMult);
            //speed = speed * speedMult;
            moveDirection *= speed;

        }
        UpdateAnimator();

        moveDirection.y -= gravity * Time.deltaTime;

        characterController.Move(moveDirection * Time.deltaTime);
    }

    void FixedUpdate()
    {
        // Mouse cursor offset effect
        playerPosOnScreen = playerCamera.WorldToViewportPoint(transform.position);
        cursorPosition = playerCamera.ScreenToViewportPoint(Input.mousePosition);
        offsetVector = cursorPosition - playerPosOnScreen;

        // mousePos = new Vector3(cursorPosition.x, 0, cursorPosition.y);
        // Debug.Log(mousePos);

        // Aim target position and rotation
        targetObject.transform.position = GetAimTargetPos();
        targetObject.transform.LookAt(new Vector3(transform.position.x, targetObject.transform.position.y, transform.position.z));

        // Player rotation
        transform.LookAt(new Vector3(targetObject.transform.position.x, transform.position.y, targetObject.transform.position.z));
    }

    private void UpdateAnimator()
    {
        float forwardBackwardsMagnitude = 0;
        float rightLeftMagnitude = 0;
        if (axisVector.magnitude > 0)
        {
            Vector3 normalizedLookingAt = mousePos - transform.position;
            normalizedLookingAt.Normalize();
            forwardBackwardsMagnitude = Mathf.Clamp(
                    Vector3.Dot(axisVector, normalizedLookingAt), -1, 1
            );

            Vector3 perpendicularLookingAt = new Vector3(
                   normalizedLookingAt.z, 0, -normalizedLookingAt.x
            );
            rightLeftMagnitude = Mathf.Clamp(
                   Vector3.Dot(axisVector, perpendicularLookingAt), -1, 1
           );

            skelAnim.SetBool("IsMoving", true);

        }
        else
        {
            skelAnim.SetBool("IsMoving", false);
        }

        // update the animator parameters
        skelAnim.SetFloat("vertical", forwardBackwardsMagnitude);
        skelAnim.SetFloat("horizontal", rightLeftMagnitude);
    }

    Vector3 GetAimTargetPos()
    {
        // Update surface plane
        surfacePlane.SetNormalAndPosition(Vector3.up/2, transform.position);

        // Create a ray from the Mouse click position
        Ray ray = playerCamera.ScreenPointToRay(Input.mousePosition);

        // Initialise the enter variable
        float enter = 0.0f;

        if (surfacePlane.Raycast(ray, out enter))
        {
            mousePos = ray.GetPoint(enter);

            //Get the point that is clicked
            Vector3 hitPoint = ray.GetPoint(enter);

            // Move your cube GameObject to the point where you clicked
            return hitPoint;
        }

        // No raycast hit, hide the aim target by moving it far away
        return new Vector3(-5000, -5000, -5000);
    }
}
