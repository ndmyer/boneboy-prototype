﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimMove : MonoBehaviour
{
    public Animator skelAnim;

    // Update is called once per frame
    void Update()
    {
        skelAnim.SetFloat("horizontal", Input.GetAxis("Horizontal"));
        skelAnim.SetFloat("vertical", Input.GetAxis("Vertical"));
    }
}
